﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    private float Ypos;
    private float Zpos;
    private float Xpos;
    public Transform playerCharacter;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Zpos = transform.position.z;
        Ypos = transform.position.y;
        Xpos = transform.position.x;

        if (playerCharacter.position.x < 1)
        {
            transform.position = new Vector3(Xpos, Ypos, Zpos);
            Debug.Log("poop");
        }
        else if (playerCharacter.position.x > 103)
        {
            transform.position = new Vector3(Xpos, Ypos, Zpos);
            Debug.Log("peep");
        }
        else
        {
            transform.position = new Vector3(playerCharacter.position.x, Ypos, Zpos);
        } 
    }
    public void ReloadScene(bool reload)
    {
        if(reload == true)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
