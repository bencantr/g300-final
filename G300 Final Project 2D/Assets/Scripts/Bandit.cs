﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bandit : MonoBehaviour {

    [SerializeField] float m_speed = 4.0f;
    [SerializeField] float m_jumpForce = 7.5f;

    public float Health = 5;
    public float CurrentHealth;
    //public int MaxHealth;
    public Image[] hearts;
    public Sprite fullHeart;
    private Death DeathScreen;

    float currentTime = 0f;
    float startingTime = 1f;
    bool TimerStarted = false;

    private Animator m_animator;
    private Rigidbody2D m_body2d;
    private Sensor_Bandit m_groundSensor;
    public GameObject Sword;
    private bool m_grounded = false;

    public bool IsInRange = false;
    public bool IsAttacking = false;

    private bool EnemyRange = false;
    private bool EnemyAttacking = false;

    private AudioSource Audio;
    public AudioClip Strike;
    public AudioClip Footstep;
    public AudioClip EnemyStrike;


    // Use this for initialization
    void Start() {
        currentTime = startingTime;
        CurrentHealth = Health;
        Audio = gameObject.GetComponent<AudioSource>();
        m_animator = GetComponent<Animator>();
        IsInRange = Sword.GetComponent<SwordController>().InRange;
        m_body2d = GetComponent<Rigidbody2D>();
        m_groundSensor = transform.Find("GroundSensor").GetComponent<Sensor_Bandit>();
        DeathScreen = FindObjectOfType<Death>();
    }

    void Update() {

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < CurrentHealth)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
        IsInRange = Sword.GetComponent<SwordController>().InRange;
        if (TimerStarted == true)
        {
            
            currentTime -= 1 * Time.deltaTime;
            if (currentTime < 0)
            {
                TimerStarted = false;
                IsAttacking = false;
                currentTime = startingTime;
                Audio.Stop();
            }
        }
        if (!m_grounded && m_groundSensor.State()) {
            m_grounded = true;
            m_animator.SetBool("Grounded", m_grounded);
        }
        if (m_grounded && !m_groundSensor.State()) {
            m_grounded = false;
            m_animator.SetBool("Grounded", m_grounded);
        }

        float inputX = Input.GetAxis("Horizontal");

        if (inputX > 0)
            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        else if (inputX < 0)
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

        m_body2d.velocity = new Vector2(inputX * m_speed, m_body2d.velocity.y);
       
        m_animator.SetFloat("AirSpeed", m_body2d.velocity.y);

        // -- Handle Animations --
        //Death
        if (CurrentHealth < 1/*Input.GetKeyDown("e")*/) {
            m_animator.SetTrigger("Death");
            m_speed = 0f;
            DeathScreen.DeathTime();

        }

        //Hurt
        else if (CurrentHealth != Health/*EnemyRange == true && EnemyAttacking == true*/)
        {
            //m_animator.SetTrigger("Hurt");
            StartCoroutine("EqualizeHealth");
            Audio.PlayOneShot(EnemyStrike, .1f);
        }
        //Attack
        else if (Input.GetMouseButtonDown(0) && TimerStarted == false) 
        {
            m_animator.SetTrigger("Attack");
            IsAttacking = true;
            //Debug.Log(IsInRange);
            TimerStart();
            Audio.PlayOneShot(Strike, .06f);
        }
        //Jump
        else if (Input.GetKeyDown("space") && m_grounded)
        {
            m_animator.SetTrigger("Jump");
            m_grounded = false;
            m_animator.SetBool("Grounded", m_grounded);
            m_body2d.velocity = new Vector2(m_body2d.velocity.x, m_jumpForce);
            m_groundSensor.Disable(0.2f);
            Audio.Stop();

        }

        //Run
        else if (Mathf.Abs(inputX) > Mathf.Epsilon)
        {
            m_animator.SetInteger("AnimState", 2);
            if (!Audio.isPlaying && m_grounded == true)
            {
                Audio.PlayOneShot(Footstep, .5f);
            }
        }
        //Idle
        else
        {
            m_animator.SetInteger("AnimState", 0);
            
        }

            
    }
    void TimerStart()
    {
        TimerStarted = true;
        
    }
    IEnumerator EqualizeHealth()
    {
        CurrentHealth = Health;
        Debug.Log("HEY IT WORKS");
        yield return new WaitForSeconds(1f);
    }
}
