﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueHolder : MonoBehaviour
{
    public string dialogue;
    private DialogueManager DMan;
    private Win Winscreen;
    private bool playerEnter;
    public bool IsLevelEnder;
    void Start()
    {
        playerEnter = false;
        DMan = FindObjectOfType<DialogueManager>();
        Winscreen = FindObjectOfType<Win>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e") && playerEnter)
        {
            if(IsLevelEnder == true)
            {
                Winscreen.WinTime();
            }
            if (IsLevelEnder == false)
            {
                Debug.Log("I get to step 2");
                DMan.ShowBox(dialogue);
            }

        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerEnter = true;
            Debug.Log("I get to step 1");
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.name == "Player")
        {
            playerEnter = false;
        }
    }
}
