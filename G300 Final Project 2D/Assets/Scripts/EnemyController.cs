﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	public float speed;
	public float health;
	public bool MoveRight;
	public GameObject Player;
	float distance;
	private bool m_IsWalking = false;
	private bool InPlayerAttackRange;
	private bool PlayerIsAttacking;
	public bool PlayerInRange;
	public bool IsAttacking;
	public bool HasAttacked = false;
	bool TimerStarted = false;
	bool dead = false;
	float currentTime = 0f;
	float startingTime = .6f;
	float despawnTime = 0f;

	// Use this for initialization

	void Start()
    {
		currentTime = startingTime;
		despawnTime = 1.2f;


	}
	void Update()
	{
		//Check for player attack
		InPlayerAttackRange = Player.GetComponent<Bandit>().IsInRange;
		distance = Vector2.Distance(gameObject.transform.position, Player.transform.position);
		PlayerIsAttacking = Player.GetComponent<Bandit>().IsAttacking;
		//Attacking timer
		if(dead)
        {
			despawnTime -= 1 * Time.deltaTime;
			if(despawnTime < 0)
            {
				Destroy(gameObject);
            }
		}
		if (TimerStarted == true)
		{
			Debug.Log(currentTime);
			HasAttacked = true;
			currentTime -= 1 * Time.deltaTime;
			gameObject.GetComponent<Animator>().SetTrigger("playerInRange");


			if (currentTime < 0)
			{
				Player.GetComponent<Bandit>().Health = Player.GetComponent<Bandit>().Health - 1;
				TimerStarted = false;
				HasAttacked = false;
				currentTime = startingTime;

			}
		}
		//Death if hit by player
		if (InPlayerAttackRange == true && PlayerIsAttacking == true && distance < 1)
        {
			TimerStarted = false;
			currentTime = startingTime;

			Die();

		}
		// check if walking, apply movement
		if (speed == 0)
		{
			m_IsWalking = false;
		}
		if (MoveRight == true && speed > 0)
		{
			transform.Translate(2 * Time.deltaTime * speed, 0, 0);
			transform.localScale = new Vector2(3, 3);
			m_IsWalking = true;
			gameObject.GetComponent<Animator>().SetBool("IsWalking", m_IsWalking);
		}
		else if (MoveRight == false && speed > 0)
		{
			transform.Translate(2 * Time.deltaTime * -speed, 0, 0);
			transform.localScale = new Vector2(-3, 3);
			m_IsWalking = true;
			gameObject.GetComponent<Animator>().SetBool("IsWalking", m_IsWalking);
		}

		
	}
	//check if player is in range to attack
	private void OnTriggerStay2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("Player"))
        {
			//attack
			gameObject.GetComponent<Animator>().SetTrigger("playerInRange");
			PlayerInRange = true;
			IsAttacking = true;
			

			if(PlayerInRange && IsAttacking && HasAttacked == false)
            {
				//Player.GetComponent<Bandit>().Health = Player.GetComponent<Bandit>().Health - 1;
				HasAttacked = true;
				TimerStarted = true;
				Debug.Log("yoo");
			}

		}
		
	}
	void OnTriggerExit2D(Collider2D collider)
    {
		IsAttacking = false;
    }
	void OnTriggerEnter2D(Collider2D collider)
    {

		if (collider.gameObject.CompareTag("turn"))
		{

			if (MoveRight)
			{
				MoveRight = false;

			}
			else
			{
				MoveRight = true;
			}
		}
	}
	void Die()
    {

		speed = 0;
		m_IsWalking = false;
		gameObject.GetComponent<Animator>().SetTrigger("isDead");
		currentTime = startingTime + 4f;
		TimerStarted = true;
		dead = true;


	}
	
}
